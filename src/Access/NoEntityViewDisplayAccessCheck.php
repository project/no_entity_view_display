<?php

namespace Drupal\no_entity_view_display\Access;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Checks access for displaying entity view display related routes.
 */
class NoEntityViewDisplayAccessCheck implements AccessInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new NoEntityViewDisplayAccessCheck.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory
  ) {
    $this->configFactory = $configFactory;
  }

  /**
   * Forbids access for view display & view mode related routes.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $routeMatch): AccessResultInterface {
    $config = $this->configFactory->get('no_entity_view_display.settings');
    $entityTypeId = $routeMatch->getParameter('entity_type_id');

    if (in_array($entityTypeId, $config->get('entity_types'))) {
      return AccessResult::forbidden('Entity view displays are disabled for this entity type.');
    }

    return AccessResult::neutral();
  }

}
