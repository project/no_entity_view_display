<?php

namespace Drupal\no_entity_view_display;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a config storage for entity view displays.
 *
 * Reads and deletes are allowed, save operations are nullified.
 */
class EntityViewDisplayStorage extends ConfigEntityStorage {

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity) {
    return SAVED_NEW;
  }

  /**
   * {@inheritdoc}
   */
  protected function doSave($id, EntityInterface $entity) {
    return SAVED_NEW;
  }

}
