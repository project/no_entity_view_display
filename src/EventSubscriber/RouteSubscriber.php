<?php

namespace Drupal\no_entity_view_display\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber for disabling entity view display/view mode related routes.
 */
class RouteSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Run after \Drupal\field_ui\Routing\RouteSubscriber.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -101];

    return $events;
  }

  /**
   * Disables entity view display/view mode related routes.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route build event.
   */
  public function onAlterRoutes(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();

    $routesToBlock = [
      'entity.entity_view_display.collection',
      'field_ui.entity_view_mode_add',
      'entity.entity_view_mode.add_form',
      'entity.entity_view_mode.edit_form',
      'entity.entity_view_mode.delete_form',
    ];

    foreach ($routesToBlock as $routeName) {
      if ($route = $collection->get($routeName)) {
        $route->addRequirements(['_no_entity_view_display' => 'TRUE']);
      }
    }

    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      $routesToBlock = [
        "entity.entity_view_display.{$entity_type_id}.default",
        "entity.entity_view_display.{$entity_type_id}.view_mode",
      ];

      foreach ($routesToBlock as $routeName) {
        if ($route = $collection->get($routeName)) {
          $route->addRequirements(['_no_entity_view_display' => 'TRUE']);
        }
      }
    }
  }

}
