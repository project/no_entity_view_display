<?php

namespace Drupal\no_entity_view_display\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Delete view displays/modes & rebuild routes when the settings are changed.
 */
class SettingsConfigSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route builder.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * SettingsConfigSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $routeBuilder
   *   The route builder.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    RouteBuilderInterface $routeBuilder
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->routeBuilder = $routeBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ConfigEvents::SAVE][] = ['onConfigSave'];

    return $events;
  }

  /**
   * Delete view displays/modes & rebuild routes when the settings are changed.
   */
  public function onConfigSave(ConfigCrudEvent $event): void {
    if ($event->getConfig()->getName() !== 'no_entity_view_display.settings') {
      return;
    }
    if (!$event->isChanged('entity_types')) {
      return;
    }

    // Delete existing view displays/modes.
    $entityTypeIds = $event->getConfig()->get('entity_types');

    foreach ($entityTypeIds as $entityTypeId) {
      foreach (['entity_view_display', 'entity_view_mode'] as $displayEntityTypeId) {
        $displayStorage = $this->entityTypeManager
          ->getStorage($displayEntityTypeId);
        $displayIds = $displayStorage->getQuery()
          ->condition('targetEntityType', $entityTypeId)
          ->execute();

        $displays = $displayStorage->loadMultiple($displayIds);
        $displayStorage->delete($displays);
      }
    }

    // Rebuild cached routes.
    $this->routeBuilder->rebuild();
  }

}
