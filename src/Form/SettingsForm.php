<?php

namespace Drupal\no_entity_view_display\Form;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A settings form for the no_entity_view_display module.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['no_entity_view_display.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('no_entity_view_display.settings');

    $entityTypes = array_filter(
      $this->entityTypeManager->getDefinitions(),
      function (EntityTypeInterface $entityType) {
        return $entityType->hasHandlerClass('view_builder')
          && $entityType->entityClassImplements(FieldableEntityInterface::class);
      }
    );

    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity types'),
      '#description' => $this->t('All entity types for which existing view displays will be deleted and the creation of new displays will be disabled.'),
      '#default_value' => $config->get('entity_types'),
      '#options' => array_map(
        function (EntityTypeInterface $entityType) {
          return $entityType->getLabel();
        },
        $entityTypes
      ),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('no_entity_view_display.settings')
      ->set('entity_types', array_keys(array_filter($form_state->getValue('entity_types'))))
      ->save();

    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'no_entity_view_display_settings';
  }

}
